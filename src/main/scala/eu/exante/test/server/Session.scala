package eu.exante.test.server

import java.net.InetSocketAddress
import java.nio.charset.StandardCharsets

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.ByteString
import eu.exante.test.core.Storage.{CandlestickCreated, CandlesticksHistory, Subscribe}
import eu.exante.test.domain.Candlestick

/**
 * Сессия клиента
 * Подписывается на события хранилища
 * Транслирует события в сообщения для клиентов
 *
 * @author Mikhail Danilov
 * 14.09.2019
 *
 */
class Session(id: String,
              storage: ActorRef,
              connection: ActorRef,
              remote: InetSocketAddress) extends Actor with ActorLogging {

  import akka.io.Tcp._

  override def preStart() {
    log.debug("subscribe")
    storage ! Subscribe
  }

  def receive: Receive = {

    case CandlesticksHistory(candlesticks) =>
      log.debug("process history {}", candlesticks)
      val str = candlesticks.map(toJson).mkString("\n") + "\n"
      val bs = ByteString.fromString(str)
      connection ! Write(bs)

    case CandlestickCreated(cs) =>
      val bs = ByteString(toJson(cs) + "\n")
      if (log.isDebugEnabled) {
        log.debug("send data {} to client {}", bs.decodeString(StandardCharsets.UTF_8), id)
      }
      connection ! Write(bs)

    case PeerClosed =>
      context.stop(self)
  }

  def toJson(cs: Candlestick): String = {
    s"""{ "ticker": "${cs.ticker}", "timestamp": "${cs.start.toString}", "open": ${cs.open}, "high": ${cs.high}, "low": ${cs.low}, "close": ${cs.close}, "volume": ${cs.volume} }"""
  }
}

object Session {

  def props(id: String, storage: ActorRef, connection: ActorRef, remote: InetSocketAddress) =
    Props(new Session(id, storage, connection, remote))

}