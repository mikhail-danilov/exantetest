package eu.exante.test.server

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, ActorRefFactory, Props}
import akka.io.{IO, Tcp}
import eu.exante.test.server.Server.SessionFactory

/**
 * Серверный актор
 *
 * Инициирует привязку к локальному адресу
 * Создает сессии при подключении клиентов
 *
 * @author Mikhail Danilov
 * 14.09.2019
 *
 */
class Server(sessionFactory: SessionFactory, bindAddress: InetSocketAddress) extends Actor {

  import Tcp._
  import context.system

  IO(Tcp) ! Bind(self, bindAddress)

  def receive: Receive = {
    case b@Bound(_) =>
      context.parent ! b

    case CommandFailed(_: Bind) => context.stop(self)

    case Connected(remote, _) =>
      val connection = sender()
      val session = sessionFactory(context, connection, remote)
      connection ! Register(session)
  }

}

object Server {

  /**
   * Функция для создания акторов-сессий
   * Вызывается при подключеии нового клиента
   */
  type SessionFactory = (ActorRefFactory, ActorRef, InetSocketAddress) => ActorRef

  def props(sessionFactory: SessionFactory, bindAddress: InetSocketAddress): Props =
    Props(new Server(sessionFactory, bindAddress))

}
