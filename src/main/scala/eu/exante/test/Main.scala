package eu.exante.test

import java.net.InetSocketAddress
import java.time.LocalTime
import java.time.temporal.ChronoUnit
import java.util.UUID

import akka.actor.ActorSystem
import eu.exante.test.core.Router.WorkerFactory
import eu.exante.test.core.{Router, Storage, Worker}
import eu.exante.test.server.Server.SessionFactory
import eu.exante.test.server.{Server, Session}
import eu.exante.test.upstream._

import scala.concurrent.duration._

/**
 * @author Mikhail Danilov
 * 12.09.2019
 *
 */
object Main extends App {

  val system: ActorSystem = ActorSystem("exante-test")

  val storageSize = system.settings.config.getInt("app.storage.size")
  val storage = system.actorOf(Storage.props(storageSize), "storage")

  val sessionFactory: SessionFactory = (actorRefFactory, connection, remoteAddr) =>
    actorRefFactory.actorOf(Session.props(UUID.randomUUID().toString, storage, connection, remoteAddr))

  val serverHost = system.settings.config.getString("app.server.host")
  val serverPort = system.settings.config.getInt("app.server.port")
  val serverBindAddr = new InetSocketAddress(serverHost, serverPort)
  val server = system.actorOf(Server.props(sessionFactory, serverBindAddr), "server")

  val workerFactory: WorkerFactory = (actorRefFactory, ticker) =>
    actorRefFactory.actorOf(Worker.props(ticker, storage), ticker)
  val router = system.actorOf(Router.props(workerFactory), "router")

  val decoder = system.actorOf(Decoder.props(router), "decoder")

  val upstreamHost = system.settings.config.getString("app.upstream.host")
  val upstreamPort = system.settings.config.getInt("app.upstream.port")
  val upstreamAddr = InetSocketAddress.createUnresolved(upstreamHost, upstreamPort)
  val client = system.actorOf(Client.props(upstreamAddr, decoder), "client")

  val delay = {
    val now = LocalTime.now()
    val time = now.truncatedTo(ChronoUnit.MINUTES).plusSeconds(60).plus(100, ChronoUnit.MILLIS)
    time.toSecondOfDay - now.toSecondOfDay
    }.seconds

  import system.dispatcher

  system.scheduler.schedule(delay, 60.seconds, router, Worker.Flush)

}
