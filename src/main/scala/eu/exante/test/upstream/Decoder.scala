package eu.exante.test.upstream

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import akka.util.ByteString
import javax.xml.bind.DatatypeConverter

import scala.util.{Failure, Success}

/**
 * Декодер сообщений
 *
 * В текущей реализации работает только Order
 *
 * @param orderProcessor обработчик ордеров
 */
class Decoder(orderProcessor: ActorRef) extends Actor with ActorLogging {

  def receive: Receive = {
    case bs: ByteString =>
      OrderDecoder.decode(bs) match {
        case Success(order) =>
          log.debug("Decoded order {}", order)
          orderProcessor ! order
        case Failure(exception) =>
          val base64string = DatatypeConverter.printBase64Binary(bs.toArray)
          log.error(exception, s"skip corrupted order $base64string")
      }
  }

}

object Decoder {

  def props(routerActor: ActorRef): Props = Props(new Decoder(routerActor))

}
