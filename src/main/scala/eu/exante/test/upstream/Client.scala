package eu.exante.test.upstream

import java.net.InetSocketAddress

import akka.actor.{Actor, ActorRef, Props}
import akka.io.{IO, Tcp}
import akka.util.ByteString

/**
 * Управляет соединением до удаленного upstream-сервера
 * Транслирует полученные сообщения в listener
 *
 * @author Mikhail Danilov
 * 13.09.2019
 *
 */
class Client(remote: InetSocketAddress, listener: ActorRef) extends Actor {

  import akka.io.Tcp._
  import context.system

  IO(Tcp) ! Connect(remote)

  def receive: Receive = {
    case CommandFailed(_: Connect) =>
      listener ! "connect failed"
      context.stop(self)

    case c@Connected(_, _) =>
      listener ! c
      val connection = sender()
      connection ! Register(self)
      context.become {
        case data: ByteString =>
          connection ! Write(data)
        case CommandFailed(_: Write) =>
          // O/S buffer was full
          listener ! "write failed"
        case Received(data) =>
          listener ! data
        case "close" =>
          connection ! Close
        case _: ConnectionClosed =>
          listener ! "connection closed"
          //todo reconnect?
          context.stop(self)
      }
  }
}

object Client {

  def props(remote: InetSocketAddress, replies: ActorRef) = Props(new Client(remote, replies))

}
