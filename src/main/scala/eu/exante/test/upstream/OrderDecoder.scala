package eu.exante.test.upstream

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import java.time.Instant

import akka.util.ByteString
import eu.exante.test.domain
import eu.exante.test.domain.Order

import scala.util.Try

/**
 * Декодер ордера из бинарного представления:
 *
 * [ LEN:2 ] [ TIMESTAMP:8 ] [ TICKER_LEN:2 ] [ TICKER:TICKER_LEN ] [ PRICE:8 ] [ SIZE:4 ]
 * где поля имеют следующую семантику:
 *
 * LEN: длина последующего сообщения (целое, 2 байта)
 * TIMESTAMP: дата и время события (целое, 8 байт, milliseconds since epoch)
 * TICKER_LEN: длина биржевого тикера (целое, 2 байта)
 * TICKER: биржевой тикер (ASCII, TICKER_LEN байт)
 * PRICE: цена сделки (double, 8 байт)
 * SIZE: объем сделки (целое, 4 байта)
 *
 * @author Mikhail Danilov
 * 19.09.2019
 *
 */
object OrderDecoder {

  private val MSG_MIN_LENGTH = 2

  /**
   * Декодирует ордер из заданного набора байт
   *
   * @param bs байты
   * @return результат декодирования (возможно отрицательный)
   */
  def decode(bs: ByteString): Try[Order] = {
    Try {
      val msg = bs.asByteBuffer
      val msgLen = msg.getShort
      require(bs.length - MSG_MIN_LENGTH == msgLen, s"message length is invalid. totalLen=${bs.length} msgLen=$msgLen")
      val timestamp = Instant.ofEpochMilli(msg.getLong)
      val ticker = getAsciiString(msg)
      val price = msg.getDouble
      val size = msg.getInt
      domain.Order(timestamp, ticker, price, size)
    }
  }

  private def getAsciiString(bb: ByteBuffer) = {
    val strLen = bb.getShort
    val strBytes = new Array[Byte](strLen)
    bb.get(strBytes)
    new String(strBytes, StandardCharsets.US_ASCII)
  }
}
