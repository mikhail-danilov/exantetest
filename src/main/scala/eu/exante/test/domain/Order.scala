package eu.exante.test.domain

import java.time.Instant

/**
 * Биржевая сделка
 *
 * @param timestamp дата и время события
 * @param ticker    биржевой тикер
 * @param price     цена сделки
 * @param size      объем сделки
 */
case class Order(timestamp: Instant, ticker: String, price: BigDecimal, size: BigDecimal) {
  require(timestamp != null, "timestamp should not be null")
  require(ticker != null && ticker.trim.length > 0, "ticker should not be blank")
  require(price > 0, "price should be positive")
  require(size > 0, "size should be positive")
  //todo более сложные правила (торговый день, перечень разрешенных инструментов, ограничения по объему/цене и тд)
}