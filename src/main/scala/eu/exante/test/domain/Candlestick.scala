package eu.exante.test.domain

import java.time.Instant
import java.time.temporal.ChronoUnit

/**
 * Структура для представлеия свечи на биржевой диаграмме
 *
 * @see https://en.wikipedia.org/wiki/Candlestick_chart
 * @author Mikhail Danilov
 * 17.09.2019
 *
 */
case class Candlestick(ticker: String,
                       start: Instant,
                       end: Instant,
                       open: BigDecimal,
                       close: BigDecimal,
                       high: BigDecimal,
                       low: BigDecimal,
                       volume: BigDecimal) {

  require(open == null || open > 0, "open price should be null or positive")
  require(close == null || close > 0, "close price should be null or positive")
  require(high == null || high > 0, "high price should be null or positive")
  require(low == null || low > 0, "low price should be null or positive")
  require(volume >= 0, "volume should be positive")
  require(start != null, "start should not be null")
  require(end != null, "end should not be null")
  require(ticker != null && ticker.trim.length > 0, "ticker should not be blank")
  require(start.isBefore(end), "start timestamp should be before end timestamp")

  /**
   * Добавляет данные ордера
   *
   * @param order ордер с соответствующим тикером и временным промежутком
   * @return новый экземпляр свечи с учетом переданного ордера
   */
  def addOrder(order: Order): Candlestick = {

    require(order.ticker == this.ticker,
      s"order ticker ${order.ticker} doesn't match candlestick ticker ${this.ticker}")

    require(order.timestamp.isBefore(this.end) && order.timestamp.isAfter(this.start),
      s"order timestamp ${order.timestamp} is out of candlestick time frame ${this.start}-${this.end}")

    Candlestick(
      this.ticker,
      this.start,
      this.end,
      if (isEmpty) order.price else this.open,
      order.price,
      if (isEmpty || order.price > this.high) order.price else this.high,
      if (isEmpty || order.price < this.low) order.price else this.low,
      this.volume + order.size)
  }

  /**
   * Свеча считается пустой если в нее не был добавлен ни один ордер
   *
   * @return true если является путой, иначе false
   */
  def isEmpty: Boolean = {
    this.open == null
  }

}

object Candlestick {

  private val durationSeconds = 60

  /**
   * Создает пустую свечу
   *
   * @param ticker    тикер
   * @param timestamp временная метка, на основе которой будут расчитаны границы
   * @return новый экземпляр пустой свечи
   */
  def empty(ticker: String, timestamp: Instant): Candlestick = {
    val (min, max) = calculateBounds(timestamp)
    Candlestick(ticker, min, max, null, null, null, null, 0)
  }

  private def calculateBounds(timestamp: Instant): (Instant, Instant) = {
    val min = timestamp.truncatedTo(ChronoUnit.MINUTES)
    val max = min.plus(durationSeconds, ChronoUnit.SECONDS)
    (min, max)
  }

  /**
   * Создает новую свечу из заданного ордера
   *
   * @param order ордер
   * @return новый экземпляр свечи
   */
  def fromOrder(order: Order): Candlestick = {
    val (min, max) = calculateBounds(order.timestamp)
    Candlestick(
      order.ticker,
      min,
      max,
      order.price,
      order.price,
      order.price,
      order.price,
      order.size)
  }
}
