package eu.exante.test.core

import akka.actor.{Actor, ActorLogging, ActorRef, Props, Terminated}
import eu.exante.test.core.Storage.{CandlestickCreated, CandlesticksHistory, Save, Subscribe}
import eu.exante.test.domain.Candlestick

import scala.collection.mutable.ListBuffer

/**
 * Хранилище свечей
 * Реализовано в виде in-memory стркутуры
 * В текущей реализации может существовать только в одном экземпляре
 *
 * @author Mikhail Danilov
 * 15.09.2019
 *
 */
class Storage(size: Integer) extends Actor with ActorLogging {

  private val subscribers = ListBuffer.empty[ActorRef]
  private var tickerToCandlesticks = Map.empty[String, ListBuffer[Candlestick]]

  override def receive: Receive = {
    case Save(cs) =>
      tickerToCandlesticks.get(cs.ticker) match {
        case Some(list) =>
          list.append(cs)
          if (list.size > size) {
            list.remove(0)
          }
        case None =>
          val list = new ListBuffer[Candlestick]()
          list.append(cs)
          tickerToCandlesticks += cs.ticker -> list
      }

      subscribers.foreach(_ ! CandlestickCreated(cs))

    case Subscribe =>
      val subscriber = sender()
      log.debug("received subscribe request {}", subscriber)
      subscribers += subscriber
      context.watch(subscriber)
      if (tickerToCandlesticks.nonEmpty) {
        subscriber ! CandlesticksHistory(candlesticks())
      }

    case Terminated(subscriber) =>
      log.debug("unsubscribe terminated {}", subscriber)
      subscribers -= subscriber

  }

  def candlesticks(): List[Candlestick] = {
    tickerToCandlesticks.values.toSeq.flatten.sortBy(cs => (cs.start, cs.ticker)).toList
  }

}

object Storage {

  def props(size: Integer): Props = Props(new Storage(size))

  /**
   * Содержит исторические данные о свечах
   * Отправляется при создании новой подписки
   *
   * @param data список свечей по всем тикерам
   */
  case class CandlesticksHistory(data: List[Candlestick])

  /**
   * Содержит информацию о созданной свече
   * Отправляется подсписчикам после сохранения очередной свечи
   *
   * @param item элемент свечи
   */
  case class CandlestickCreated(item: Candlestick)

  /**
   * Подписаться на события хранилища
   */
  case object Subscribe

  /**
   * Сохранить данные свечи
   *
   * @param item свеча для сохранения
   */
  case class Save(item: Candlestick)

}
