package eu.exante.test.core

import java.time.{Clock, Instant}

import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import eu.exante.test.core.Storage.Save
import eu.exante.test.core.Worker.Flush
import eu.exante.test.domain.{Candlestick, Order}

/**
 * Обработчик ордеров по заданному тикеру
 * <p>
 * Накапливает ордеры только по одной, текущей свече
 * Как только поступает ордер с временем из следующего временного промежутка,
 * свеча отправляется в хранилище (Storage) и создается новая.
 * <p>
 * На случай если долго не приходят ордера, ожидает команду Flush для отправкисвечи из завершенного временого интервала
 * <p>
 * Если за временной промежуток не было ордеров, то свеча не будет отправлена на сохранение
 *
 * @author Mikhail Danilov
 * 14.09.2019
 *
 */
class Worker(ticker: String, storage: ActorRef, clock: Clock)
  extends Actor with ActorLogging {

  override def receive: Receive = updated(Candlestick.empty(ticker, Instant.now(clock)))

  def updated(candlestick: Candlestick): Receive = {
    case order: Order =>

      if (order.ticker != ticker) {
        log.error("order ticker doesn't match {} {}", order, ticker)
        throw new IllegalArgumentException("order ticker doesn't match")
      }

      order.timestamp match {

        case orderTimestamp if orderTimestamp.isBefore(candlestick.start) =>
          log.warning("received order from past time frame {}", order)

        case orderTimestamp if orderTimestamp.isAfter(candlestick.end) =>
          log.debug("received order from next time frame {}", order)
          storage ! Save(candlestick)
          context become updated(Candlestick.fromOrder(order))

        case _ =>
          log.debug("received order from current time frame {}", order)
          context become updated(candlestick.addOrder(order))
      }
    case Flush =>
      log.debug("received flush command")
      val now = Instant.now(clock)
      if (now.isAfter(candlestick.end)) {
        if (!candlestick.isEmpty) {
          storage ! Save(candlestick)
        }
        context become updated(Candlestick.empty(ticker, now))
      }
  }

}

object Worker {

  def props(ticker: String, storage: ActorRef, clock: Clock = Clock.systemUTC()) =
    Props(new Worker(ticker, storage, clock))

  /**
   * Событие для отправки на сохранение свечи из завершенного временного интервала
   */
  case object Flush

}