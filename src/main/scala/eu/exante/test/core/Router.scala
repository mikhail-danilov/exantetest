package eu.exante.test.core

import akka.actor.{Actor, ActorLogging, ActorRef, ActorRefFactory, Props}
import eu.exante.test.core.Router.WorkerFactory
import eu.exante.test.core.Worker.Flush
import eu.exante.test.domain.Order


/**
 * Маршрутизатор ордеров
 * На каждый тикер создает обработчик
 * Определяет в какой обработчик (Worker) отправить полученный ордер
 *
 * @author Mikhail Danilov
 * 13.09.2019
 *
 */
class Router(workerFactory: WorkerFactory) extends Actor with ActorLogging {

  private var tickerToWorker = Map.empty[String, ActorRef]

  def receive: Receive = {
    case order: Order =>
      tickerToWorker.get(order.ticker) match {
        case Some(tickerActor) =>
          tickerActor.forward(order)
        case None =>
          log.debug("creating worker actor for {}", order.ticker)
          val tickerActor = workerFactory(context, order.ticker)
          tickerToWorker += order.ticker -> tickerActor
          tickerActor.forward(order)
      }
    case Flush =>
      log.debug("broadcast flush stale candlesticks")
      tickerToWorker.values.foreach(tickerActor => tickerActor.forward(Flush))
  }
}

object Router {

  /**
   * Функция создания дочерних акторов по обработке ордеров в разрезе тикров
   */
  type WorkerFactory = (ActorRefFactory, String) => ActorRef

  def props(childFactory: WorkerFactory): Props = Props(new Router(childFactory))

}
