package eu.exante.test.upstream

import java.nio.file.{Files, Paths}
import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import akka.util.ByteString
import eu.exante.test.domain.Order
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

/**
 * @author Mikhail Danilov
 * 18.09.2019
 *
 */
class DecoderSpec
  extends TestKit(ActorSystem("exante-test"))
    with WordSpecLike
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A Decoder actor" must {

    "decode a valid byte message to Order" in {

      //Given
      val routerProbe = TestProbe("router")
      val decoder = system.actorOf(Decoder.props(routerProbe.ref))
      val msg = readResource("/msg.dat")

      //When
      decoder ! ByteString.fromArray(msg)

      //Then
      routerProbe.expectMsg(Order(Instant.parse("2019-09-18T13:17:06.895Z"), "GOOG", 91.6, 900))

    }

    "ignore when msg is corrupted" in {

      //Given
      val routerProbe = TestProbe("router")
      val decoder = system.actorOf(Decoder.props(routerProbe.ref))
      val msg = ByteString.fromString("12345")

      //When
      decoder ! msg

      //Then
      routerProbe.expectNoMessage()

    }

    "ignore when msg is empty" in {

      //Given
      val routerProbe = TestProbe("router")
      val decoder = system.actorOf(Decoder.props(routerProbe.ref))
      val msg = ByteString.empty

      //When
      decoder ! msg

      //Then
      routerProbe.expectNoMessage()

    }

    def readResource(path: String): Array[Byte] = {
      Option(path)
        .map(getClass.getResource)
        .map(_.toURI)
        .map(Paths.get)
        .map(Files.readAllBytes)
        .get
    }


  }
}
