package eu.exante.test.core

import java.time.{Clock, Instant, ZoneOffset, Duration => JDuration}

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import com.mercateo.test.clock.TestClock
import eu.exante.test.core.Storage.Save
import eu.exante.test.core.Worker.Flush
import eu.exante.test.domain.{Candlestick, Order}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

/**
 * @author Mikhail Danilov
 * 18.09.2019
 *
 */
class WorkerSpec
  extends TestKit(ActorSystem("exante-test"))
    with WordSpecLike
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A Worker actor" must {

    "process candlestick depending on time frame" in {

      //Given
      val storageProbe = TestProbe()
      val clock = Clock.fixed(Instant.parse("2019-09-18T01:30:05Z"), ZoneOffset.UTC)
      val worker = system.actorOf(Worker.props("AAPL", storageProbe.ref, clock))

      //When

      //past frame
      worker ! Order(Instant.parse("2019-09-18T01:29:15Z"), "AAPL", 113.11, 15)
      //current frame
      worker ! Order(Instant.parse("2019-09-18T01:30:10Z"), "AAPL", 100.31, 10)
      worker ! Order(Instant.parse("2019-09-18T01:30:15Z"), "AAPL", 109.18, 23)
      //future frame
      worker ! Order(Instant.parse("2019-09-18T01:31:15Z"), "AAPL", 90.55, 3)

      //Then
      val expectedCandlestick = Candlestick("AAPL",
        Instant.parse("2019-09-18T01:30:00Z"),
        Instant.parse("2019-09-18T01:31:00Z"),
        100.31,
        109.18,
        109.18,
        100.31,
        33)

      storageProbe.expectMsg(Save(expectedCandlestick))
    }

    "not save candlestick if no orders where in time frame" in {

      //Given
      val storageProbe = TestProbe()
      val clock = TestClock.fixed(Instant.parse("2019-09-18T01:30:05Z"), ZoneOffset.UTC)
      val worker = system.actorOf(Worker.props("AAPL", storageProbe.ref, clock))

      //When
      Thread.sleep(1000)
      clock.fastForward(JDuration.ofMinutes(1))
      worker ! Flush

      //Then
      storageProbe.expectNoMessage()
    }

    "ignore when receive order with unexpected ticker" in {

      //Given
      val storageProbe = TestProbe()
      val testClock = TestClock.fixed(Instant.parse("2019-09-18T01:30:05Z"), ZoneOffset.UTC)
      val worker = system.actorOf(Worker.props("AAPL", storageProbe.ref, testClock))
      val watchProbe = TestProbe()
      watchProbe.watch(worker)

      //When
      worker ! Order(Instant.parse("2019-09-18T01:29:15Z"), "MSFT", 113.11, 15)

      //Then
      Thread.sleep(1000)
      testClock.fastForward(JDuration.ofMinutes(1))
      worker ! Flush
      storageProbe.expectNoMessage()
    }

  }
}
