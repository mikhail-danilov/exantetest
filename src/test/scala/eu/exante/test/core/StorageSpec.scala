package eu.exante.test.core

import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import eu.exante.test.core.Storage.{CandlestickCreated, CandlesticksHistory, Save, Subscribe}
import eu.exante.test.domain.Candlestick
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

import scala.collection.mutable.ListBuffer

/**
 * @author Mikhail Danilov
 * 18.09.2019
 *
 */
class StorageSpec
  extends TestKit(ActorSystem("exante-test"))
    with WordSpecLike
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A Storage actor" must {

    "save only last 10 candlesticks per ticker" in {

      //Given
      val sessionProbe = TestProbe("session")
      val storage = system.actorOf(Storage.props(10))
      val candlesticks = new ListBuffer[Candlestick]()

      //When
      for {sec <- 30 until 42; ticker <- Array("MSFT", "AAPL")} {
        val cs = Candlestick(s"$ticker", Instant.parse(s"2019-09-18T01:$sec:00Z"),
          Instant.parse(s"2019-09-18T01:${sec + 1}:00Z"), 100.31, 109.18, 109.18, 100.31, 33)
        candlesticks append cs
        storage ! Save(cs)
      }
      sessionProbe.send(storage, Subscribe)

      //Then
      val expected = candlesticks.toList
        .filter(_.start.isAfter(Instant.parse(s"2019-09-18T01:31:00Z")))
        .sortBy(cs => (cs.start, cs.ticker))

      sessionProbe.expectMsg(CandlesticksHistory(expected))

    }

    "send candlesticks to subscribers" in {

      //Given
      val sessionProbe = TestProbe("session")
      val storage = system.actorOf(Storage.props(10))

      //When

      val cs1 = Candlestick("AAPL", Instant.parse(s"2019-09-18T01:30:00Z"),
        Instant.parse(s"2019-09-18T01:31:00Z"), 100.31, 109.18, 109.18, 100.31, 33)
      storage ! Save(cs1)

      sessionProbe.expectNoMessage()

      sessionProbe.send(storage, Subscribe)

      sessionProbe.expectMsg(CandlesticksHistory(List(cs1)))

      val cs2 = Candlestick("AAPL", Instant.parse(s"2019-09-18T01:30:00Z"),
        Instant.parse(s"2019-09-18T01:31:00Z"), 100.31, 109.18, 109.18, 100.31, 33)
      storage ! Save(cs2)

      sessionProbe.expectMsg(CandlestickCreated(cs2))

    }

  }
}
