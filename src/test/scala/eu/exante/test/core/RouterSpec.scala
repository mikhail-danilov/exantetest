package eu.exante.test.core

import java.time.Instant

import akka.actor.ActorSystem
import akka.testkit.{TestKit, TestProbe}
import eu.exante.test.domain.Order
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

/**
 * @author Mikhail Danilov
 * 18.09.2019
 *
 */
class RouterSpec
  extends TestKit(ActorSystem("exante-test"))
    with WordSpecLike
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A Router actor" must {

    "create single worker per ticker" in {

      //Given
      val decoderProbe = TestProbe("decoder")
      val children = Map("AAPL" -> TestProbe(), "MSFT" -> TestProbe())

      val router = system.actorOf(Router.props((_, ticker) => children(ticker).ref))

      //When
      decoderProbe.send(router, Order(Instant.now(), "AAPL", 100.12, 33))
      decoderProbe.send(router, Order(Instant.now(), "MSFT", 100.12, 33))
      decoderProbe.send(router, Order(Instant.now(), "AAPL", 100.12, 33))

      //Then
      children("MSFT").receiveN(1)
      children("AAPL").receiveN(2)
    }

  }
}
