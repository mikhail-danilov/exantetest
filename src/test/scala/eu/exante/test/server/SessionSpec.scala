package eu.exante.test.server

import java.net.InetSocketAddress
import java.time.Instant

import akka.actor.ActorSystem
import akka.io.Tcp.Write
import akka.testkit.{TestKit, TestProbe}
import akka.util.ByteString
import eu.exante.test.core.Storage.{CandlestickCreated, CandlesticksHistory, Subscribe}
import eu.exante.test.domain.Candlestick
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

/**
 * @author Mikhail Danilov
 * 18.09.2019
 *
 */
class SessionSpec
  extends TestKit(ActorSystem("exante-test"))
    with WordSpecLike
    with BeforeAndAfterAll {

  override def afterAll: Unit = {
    TestKit.shutdownActorSystem(system)
  }

  "A Session actor" must {

    "subscribe to storage on start" in {

      //Given
      val storageProbe = TestProbe("storage")
      val connectionProbe = TestProbe("connection")

      //When
      system.actorOf(Session.props("test-123", storageProbe.ref, connectionProbe.ref,
        InetSocketAddress.createUnresolved("not.exists", 7777)))

      //Then
      storageProbe.expectMsg(Subscribe)
    }

    "send candlestick when receive CandlestickCreated" in {

      //Given
      val storageProbe = TestProbe("storage")
      val connectionProbe = TestProbe("connection")

      val cs = Candlestick("AAPL", Instant.parse(s"2019-09-18T01:30:00Z"),
        Instant.parse(s"2019-09-18T01:31:00Z"), 100.31, 109.18, 109.18, 100.31, 33)

      val session = system.actorOf(Session.props("test-123", storageProbe.ref, connectionProbe
        .ref, InetSocketAddress.createUnresolved("not.exists", 7777)))

      //When
      storageProbe.send(session, CandlestickCreated(cs))

      val expectedJson = """{ "ticker": "AAPL", "timestamp": "2019-09-18T01:30:00Z", "open": 100.31, "high": 109.18, "low": 100.31, "close": 109.18, "volume": 33 }""" + "\n"
      //Then
      connectionProbe.expectMsg(Write(ByteString(expectedJson)))
    }

    "send multiple candlesticks when receive CandlestickHistory" in {

      //Given
      val storageProbe = TestProbe("storage")
      val connectionProbe = TestProbe("connection")

      val cs1 = Candlestick("AAPL", Instant.parse(s"2019-09-18T01:30:00Z"),
        Instant.parse(s"2019-09-18T01:31:00Z"), 100.31, 109.18, 109.18, 100.31, 33)

      val cs2 = Candlestick("MSFT", Instant.parse(s"2019-09-18T01:30:00Z"),
        Instant.parse(s"2019-09-18T01:31:00Z"), 100.31, 109.18, 109.18, 100.31, 33)

      val session = system.actorOf(Session.props("test-123", storageProbe.ref, connectionProbe
        .ref, InetSocketAddress.createUnresolved("not.exists", 7777)))

      //When
      storageProbe.send(session, CandlesticksHistory(List(cs1, cs2)))

      val expectedJson1 = """{ "ticker": "AAPL", "timestamp": "2019-09-18T01:30:00Z", "open": 100.31, "high": 109.18, "low": 100.31, "close": 109.18, "volume": 33 }""" + "\n"
      val expectedJson2 = """{ "ticker": "MSFT", "timestamp": "2019-09-18T01:30:00Z", "open": 100.31, "high": 109.18, "low": 100.31, "close": 109.18, "volume": 33 }""" + "\n"
      //Then
      connectionProbe.expectMsg(Write(ByteString(expectedJson1 + expectedJson2)))
    }

  }
}
