package eu.exante.test.domain

import java.time.Instant

import org.scalatest.WordSpec

/**
 * @author Mikhail Danilov
 * 19.09.2019
 *
 */
class CandlestickSpec extends WordSpec {

  "Candlestick.addOrder" must {

    "do correct math when order is valid" in {

      val orderTimestamp = Instant.parse("2019-09-19T16:56:30.000Z")
      val from = Instant.parse("2019-09-19T16:56:00.000Z")
      val to = Instant.parse("2019-09-19T16:57:00.000Z")
      val ticker = "AAPL"
      var cs = Candlestick.empty(ticker, orderTimestamp)

      cs = cs.addOrder(Order(orderTimestamp, ticker, 50.5, 10))
      assert(cs === Candlestick(ticker, from, to, 50.5, 50.5, 50.5, 50.5, 10))

      cs = cs.addOrder(Order(orderTimestamp.plusSeconds(1), ticker, 100.5, 10))
      cs = cs.addOrder(Order(orderTimestamp.plusSeconds(2), ticker, 10.5, 10))
      cs = cs.addOrder(Order(orderTimestamp.plusSeconds(3), ticker, 32.5, 10))
      assert(cs === Candlestick(ticker, from, to, 50.5, 32.5, 100.5, 10.5, 40))

    }

    "throw IllegalArgumentException when order with different ticker" in {

      val cs = Candlestick.empty("AAPL", Instant.parse("2019-09-19T16:56:00.000Z"))
      intercept[IllegalArgumentException] {
        cs.addOrder(Order(Instant.parse("2019-09-19T16:56:30.000Z"), "MSFT", 100.5, 10))
      }
    }

    "throw IllegalArgumentException when order out of time frame" in {

      val cs = Candlestick.empty("AAPL", Instant.parse("2019-09-19T16:56:00.000Z"))
      intercept[IllegalArgumentException] {
        cs.addOrder(Order(Instant.parse("2019-09-19T16:57:30.000Z"), "AAPL", 100.5, 10))
      }

      intercept[IllegalArgumentException] {
        cs.addOrder(Order(Instant.parse("2019-09-19T16:55:30.000Z"), "AAPL", 100.5, 10))
      }
    }

  }

  "Candlestick constructor" must {

    val validCs = Candlestick("AAPL", Instant.now(), Instant.now().plusSeconds(10), 10, 20, 30, 5, 100)

    "throw IllegalArgumentException when negative numbers" in {

      intercept[IllegalArgumentException] {
        validCs.copy(open = -1)
      }
      intercept[IllegalArgumentException] {
        validCs.copy(close = -1)
      }
      intercept[IllegalArgumentException] {
        validCs.copy(high = -1)
      }
      intercept[IllegalArgumentException] {
        validCs.copy(low = -1)
      }
      intercept[IllegalArgumentException] {
        validCs.copy(volume = -1)
      }
    }

    "throw IllegalArgumentException when time bounds incorrect" in {
      intercept[IllegalArgumentException] {
        validCs.copy(start = Instant.now(), end = Instant.now().minusSeconds(60))
      }
      intercept[IllegalArgumentException] {
        validCs.copy(start = Instant.now().plusSeconds(60), end = Instant.now())
      }
      intercept[IllegalArgumentException] {
        val now = Instant.now()
        validCs.copy(start = now, end = now)
      }
    }

  }

}
