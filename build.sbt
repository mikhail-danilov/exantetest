name := "ExanteTest"

version := "1.0"

scalaVersion := "2.12.10"

enablePlugins(JavaAppPackaging)

lazy val akkaVersion = "2.5.25"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion % Test,
  "org.scalatest" %% "scalatest" % "3.0.5" % Test,
  "com.mercateo" % "test-clock" % "1.0.2" % Test
)
