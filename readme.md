# Тестовое задание EXANTE

## Описание
Есть некий сервис, который отдает нам поток биржевых данных. Мы хотели бы раздавать этот поток другим внутренним клиентам, заодно агрегируя данные в формат, достаточный для отрисовки candlestick chart. Нужно реализовать сервер для этого на Scala или Java. Желательно использование фреймворков, облегчающих написание сетевых серверов (Netty, Mina, Akka IO).
[Детали](https://bitbucket.org/snippets/exante/7j8nn)

## Требования
1. JDK8+
2. SBT 1.3.0
3. Scala 2.12

## Запуск
1. `python scripts/upstream.py`
2. `sbt run`

## Сборка
Средствами [SBT Native Packager](https://www.scala-sbt.org/sbt-native-packager/index.html)

Пример для получения zip-архива:

1. `sbt universal:packageBin`
2. `ls target/universal/exantetest-1.0.zip`

## Архитектура

Сервис реализован средствами Akka (Actors, IO).

![Диаграмма взаимодействия](uml/actors.png)
